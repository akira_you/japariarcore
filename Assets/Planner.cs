﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using UnityEngine.Events;

public class Planner : MonoBehaviour
{
    public GameObject MainCam;
    public UnityEngine.UI.Text PlanText;
    private int frameCount = 0;
    private float goAngle = 0;
    private float speed = 0;

    private bool pathFound = false;
    public int NowTarget = -1;
    
    private void Vec3To2(Vector3 v3, out Vector2 v2)
    {
        v2.x = v3.x;
        v2.y = v3.z;
    }
    const float trajStep = 0.3f;
    private Vector3[] angleToTrj(float angle)
    {
        const float step = trajStep;
        const int nofStep = 10;
        const float camToRear = 0.5f;
        const float frontToRear = 0.4f;
        Vector3[] ret = Enumerable.Repeat<Vector3>(new Vector3(), nofStep).ToArray(); ;
        Vector3 basePos = MainCam.transform.position;
        //ret[0] = basePos;
        Quaternion baseRot = MainCam.transform.rotation;
        Vector3 erot = baseRot.eulerAngles;
        erot.Scale(Vector3.up);
        baseRot = Quaternion.Euler(erot);
        basePos.y = 0;// yAve;
        basePos -= baseRot * Vector3.forward * camToRear;//Center is rea wheel
        if (Math.Abs(angle) > 0.001)
        {
            float clen = frontToRear / Mathf.Sin(angle / 180 * Mathf.PI);//length from rotation center
            float diffAngle = 360 * step / (Mathf.PI * clen * 2);
            Vector3 diffPos = new Vector3(
                Mathf.Cos(diffAngle / 180 * Mathf.PI) * clen - clen,
                0,
                Mathf.Sin(diffAngle / 180 * Mathf.PI) * clen
                );

            for (int i = 0; i < nofStep; i++)
            {
                basePos += baseRot * diffPos;
                erot.y += diffAngle; baseRot = Quaternion.Euler(erot);
                ret[i] = basePos + baseRot * Vector3.forward * camToRear;
            }
        }
        else
        {
            for (int i = 0; i < nofStep; i++)
            {
                basePos += baseRot * (step * Vector3.forward);
                ret[i] = basePos + baseRot * Vector3.forward * camToRear;
            }
        }
        return ret;

    }
    public bool ShowState = false;
    public bool MotorOn = false;



    public RouteData routeData;
    List<Vector2> relPos = new List<Vector2>();
    List<Vector2> relDirec = new List<Vector2>();


    private void SyncRouteData()
    {
        Vector3[] route = routeData.GetCourse();
        relPos.Clear();
        relDirec.Clear();
        for (int i = 0; i < route.Length; i++)
        {
            Vector3 pre = route[(i - 1 + route.Length) % route.Length];
            Vector3 now = route[i];
            Vector3 post = route[(i + 1) % route.Length];
            Vector3 d1 = now - pre;
            Vector3 d2 = post - now;
            d1 /= d1.magnitude;
            d2 /= d2.magnitude;
            Vector3 relDirec3 = MainCam.transform.InverseTransformDirection(d1 + d2);
            relDirec.Add(new Vector2(relDirec3.x, relDirec3.z));
            Vector3 relPos3 = MainCam.transform.InverseTransformPoint(now);
            relPos.Add(new Vector2(relPos3.x, relPos3.z));
        }
    }
    private string planString = "";
    private int FindFirst()
    {
        //forward: Y+ (Vector2.Up)
        //right: X+
        float minLen = 1.0e+10f;
        int minLenId = -1;
        for (int i = 0; i < relPos.Count; i++)
        {
            if (relPos[i].y < 0) continue;
            if (relDirec[i].y < 0) continue;
            float len = relPos[i].magnitude;
            if (len < minLen)
            {
                minLen = len;
                minLenId = i;
            }
        }
        if (minLenId < 0) planString += "Can not FindFirst ";
        return minLenId;
    }


    public float MaxR = 0.5f;
    public float StareR = 0.3f;
    void doPlan()
    {
        pathFound = false;
        if (NowTarget < 0) NowTarget = FindFirst();
        if (NowTarget < 0)return;
        
        //Have pass the NowTarget?
        //Just judge by relPos.x , if you need stare
        if (relPos[NowTarget].y < MaxR / 4.0 && Math.Abs(relPos[NowTarget].magnitude) < MaxR * 2)
        {
            NowTarget++;
            NowTarget %= relPos.Count;
            planString += "Go Next Target ";
        }


        float margineRate = 1;
        //always to 
        if (relPos[NowTarget].y < 0)
        {
            margineRate = 0;
            planString += " OVER ";
        }

        goAngle = 0;
        if (relPos[NowTarget].x < -StareR*margineRate)
        {
            goAngle = -20;
            planString += "LEFT     ";
        }
        if (relPos[NowTarget].x > StareR*margineRate)
        {
            planString += "RIGHT    ";
            goAngle = 20;
        }
        planString += "Target=" + (NowTarget + 1).ToString() + "  " + relPos[NowTarget].ToString() + "   ";

        pathFound = true;

    }








    private UdpClient udpclient;
    // Use this for initialization
    void Start()
    {
        udpclient = new UdpClient(13530);
        udpclient.EnableBroadcast = true;

    }

    struct LidarData
    {
        public int[] L;
    };
    void GetUDPMess()
    {
        if (udpclient.Available < 1) return;
        IPEndPoint remoteEP = null;
        string json = Encoding.ASCII.GetString(udpclient.Receive(ref remoteEP));
        LidarData data=JsonUtility.FromJson<LidarData>(json);
        int N = data.L.Length;
        LidarLine.positionCount = N/2;
  

        for(int n = N/2; n < N; n++)
        {
            float l = data.L[n] / 1000.0f;
            if (l > 7) l = 7;
            float deg = -2*Mathf.PI * (n + 0.5f) / N;
            float y = Mathf.Sin(deg)*l;
            float x = Mathf.Cos(deg)*l;
            LidarLine.SetPosition(n-N/2, new Vector3(x, 0, y));

        }


    }

    public LineRenderer GoLine;
    public LineRenderer LidarLine;
    private const float CenterDeg=94;
    private float elaspedTime = 0;
    private float oldS = CenterDeg;
    public class AccEvent :UnityEvent<float,float> { }
    public AccEvent accEvent=new AccEvent();
    // Update is called once per frame
    void Update()
    {
        GetUDPMess();    
        elaspedTime += Time.deltaTime;
        if (elaspedTime > 0.1)
        {
            elaspedTime = 0;
            planString = "plan:";
            SyncRouteData();
            doPlan();
            if (pathFound && MotorOn)
            {
                speed += 0.05f;
                if (speed > 1) speed = 1;
            }
            else
            {
                speed -= 0.1f;
                if (speed < 0) speed = 0;
            }

            float s = CenterDeg;
            if (MotorOn) s += goAngle * -1.85f;
            /*
            if (Mathf.Abs(s - oldS) > 1 && Mathf.Abs(s - oldS) < 20)
            {
                float sig = Mathf.Sign(oldS - s);
                oldS = s;
                s += sig * 20;
            }
            else
            {
                oldS = s;
            }*/

            float a = speed * 5;
            string data = a.ToString() + " " + s.ToString() + "\n";
            accEvent.Invoke(a, goAngle);
            //Debug.Log(data);
            byte[] bytes = Encoding.UTF8.GetBytes(data);
            udpclient.Send(bytes, bytes.Length, "192.168.1.255", 13531);
            udpclient.Send(bytes, bytes.Length, "192.168.43.255", 13531);
            udpclient.Send(bytes, bytes.Length, "255.255.255.255", 13531);
            if (!MotorOn) planString += "  ON READY";
            if (ShowState) PlanText.text = planString;
            else PlanText.text = "";

        }
        frameCount++;

        //CAUTION angle unit is Degree !!!!
        if (pathFound && ShowState)
        {
            Vector3[] v = angleToTrj(goAngle);
            GoLine.positionCount = v.Length;
            GoLine.SetPositions(v);
        }
        else
        {
            GoLine.positionCount = 0;

        }

    }
}
