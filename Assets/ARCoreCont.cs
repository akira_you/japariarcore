﻿
using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.HelloAR;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Events;
using UnityEngine.EventSystems;


#if UNITY_EDITOR
// Set up touch input propagation while using Instant Preview in the editor.
using Input =GoogleARCore.InstantPreviewInput;
#endif

/// <summary>
/// Controls the HelloAR example.
/// </summary>
public class ARCoreCont : MonoBehaviour 
{
    public Camera FirstPersonCamera;
    public GameObject TrackedPlanePrefab;
    public GameObject AnchorPrefab;
    public RouteData routeData;

    public UnityEvent OnLost;
    public UnityEvent OnReady;
    
    private List<TrackedPlane> m_NewPlanes = new List<TrackedPlane>();

    

    private bool m_IsQuitting = false; // Serius error happend, Can not recover 
    private bool ready = false; //tracking state
    public const int lostTrackingSleepTimeout = 15;
    int anchorLayer;

    public enum Modes { ANCHOR, FLAG, NONE }
    private Modes _mode = Modes.ANCHOR;
    public Modes Mode
    {
        get
        {
            return _mode;
        }
        set
        {
            _mode = value;
        }
    }



    private void Awake()
    {
        OnLost = new UnityEvent();
        OnReady = new UnityEvent();
        anchorLayer = LayerMask.GetMask(new string[] { "Anchor" });
        
    }
    private void DelayedQuit()
    {
        m_IsQuitting = true;
        Invoke("_DoQuit", 0.5f);
        OnLost.Invoke();

    }


    public bool AddFlag(Vector3 pos)
    {
        return routeData.AddFlag(pos);
    }

    private void OnTouchForFlag(Touch touch)
    {
        if (!AddFlag(FirstPersonCamera.transform.position))
        {
            _ShowAndroidToastMessage("No anchor near here!");

        }

    }
    private void OnTouchForAnchor(Touch touch)
    {
        //////////////////////
        //Remove Ancchor
        //////////////////////
        {
            RaycastHit hit;
            if (Physics.Raycast(FirstPersonCamera.ScreenPointToRay(touch.position), out hit,10.0f,anchorLayer ))
            {
                GameObject anchorObj = hit.collider.gameObject;
                //anchorObj.transform.parent.gameObject is the anchor
                GameObject a = anchorObj.transform.parent.gameObject;
                if ( (a != null) && (a.GetComponent<Anchor>() != null))
                {
                    Destroy(anchorObj);
                    Destroy(a);
                    return;// Delete complete
                }
            }
        }
        //////////////////////
        //Create Ancchor
        //////////////////////
        {
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
            {
                GameObject anchorObj = Instantiate(AnchorPrefab, hit.Pose.position, hit.Pose.rotation);
                Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);
                if ((hit.Flags & TrackableHitFlags.PlaneWithinPolygon) != TrackableHitFlags.None)
                {
                    Vector3 cameraPositionSameY = FirstPersonCamera.transform.position;
                    cameraPositionSameY.y = hit.Pose.position.y;
                    anchorObj.transform.LookAt(cameraPositionSameY, anchorObj.transform.up);
                }
                anchorObj.transform.parent = anchor.transform;
            }
        }
    }

    private void OnTouch(Touch touch)
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;
        if (EventSystem.current.IsPointerOverGameObject(0)) return;

        if (Mode == Modes.ANCHOR) OnTouchForAnchor(touch);
        if (Mode == Modes.FLAG) OnTouchForFlag(touch);


    }
    public void Update()
    {
        if (Input.GetKey(KeyCode.Escape)) DelayedQuit();
        _QuitOnConnectionErrors();
        if (m_IsQuitting) return;

        // Check that motion tracking is tracking.
        if (Session.Status != SessionStatus.Tracking)
        {
            if (ready)
            {
                Screen.sleepTimeout = lostTrackingSleepTimeout;
                OnLost.Invoke();
                ready = false;
            }
            return;
        }
        if (!ready)
        {
            OnReady.Invoke();
            ready = true;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

        // Iterate over planes found in this frame and instantiate corresponding GameObjects to visualize them.
        Session.GetTrackables<TrackedPlane>(m_NewPlanes, TrackableQueryFilter.New);
        for (int i = 0; i < m_NewPlanes.Count; i++)
        {
            GameObject planeObject = Instantiate(TrackedPlanePrefab, Vector3.zero, Quaternion.identity,
                transform);
            planeObject.GetComponent<TrackedPlaneVisualizer>().Initialize(m_NewPlanes[i]);
        }
        //Touch the screen
        Touch touch;
        if (Input.touchCount  >= 1 && (touch = Input.GetTouch(0)).phase == TouchPhase.Began)
        {
            OnTouch(touch);
        }

    }

    /// <summary>
    /// Quit the application if there was a connection error for the ARCore session.
    /// </summary>
    private void _QuitOnConnectionErrors()
    {
        if (m_IsQuitting) return;
            
        // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
        if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
        {
            _ShowAndroidToastMessage("Camera permission is needed to run this application.");
            DelayedQuit();
        }
        else if (Session.Status.IsError())
        {
            _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                
            DelayedQuit();
        }
    }

    /// <summary>
    /// Actually quit the application.
    /// </summary>
    private void _DoQuit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Show an Android toast message.
    /// </summary>
    /// <param name="message">Message string to show in the toast.</param>
    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }
}

