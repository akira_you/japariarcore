﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;


public class RouteData : MonoBehaviour {
    private List<GameObject> FlagObjs = new List<GameObject>();
    private Vector3[] Points = { };

    public float MaxFlagToAnchoDistance = 3.0f;
    public GameObject FlagPrefab;
    public LineRenderer CourseLine;
    private void DrawLine()
    {
        CourseLine.positionCount = FlagObjs.Count;
        if (Points.Length != FlagObjs.Count)
        {
            Points = new Vector3[FlagObjs.Count];

        }

        for (int i = 0; i < CourseLine.positionCount; i++)
        {
            Points[i] = FlagObjs[i].transform.position;
            CourseLine.SetPositions(Points);
        }
    }
    public Vector3[] GetCourse(){
        return Points;
    }

    public bool AddFlag(Vector3 pos)
    {
        int AnchorCount = 0;
        GameObject flagObj = Instantiate(FlagPrefab, pos, Quaternion.identity);
        flagObj.transform.position = pos;
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Anchor"))
        {
            if ((g.transform.position - pos).magnitude < MaxFlagToAnchoDistance &&
                g.transform.parent != null &&
                g.transform.parent.GetComponent<Anchor>() != null)
            {
                //adding anchor to flag
                flagObj.GetComponent<PoseCorrector>().AddAnchor(g.transform.parent.gameObject);
                AnchorCount++;
            }
        }

        if (AnchorCount < 1)
        {
            DestroyObject(flagObj);
            return false;
        }
        flagObj.transform.Find("NumberText").GetComponent<TextMesh>().text=( FlagObjs.Count + 1).ToString();
        FlagObjs.Add(flagObj);
        DrawLine();
        return true;
    }
    public void RemoveLastFlag()
    {
        Destroy(FlagObjs[FlagObjs.Count - 1]);
        FlagObjs.RemoveAt(FlagObjs.Count - 1);
        DrawLine();
    }
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    int frameCount = 0;
	void Update () {
        frameCount++;
        if (0 == frameCount % 4) DrawLine();
		
	}
}
