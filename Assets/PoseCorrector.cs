﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;

class CpmareInstanceID : IComparer<GameObject>
{
    public int Compare(GameObject a, GameObject b)
    {
        return b.GetInstanceID() - a.GetInstanceID();
    }
}
public class PoseCorrector : MonoBehaviour {
    private List<GameObject> anchors=new List<GameObject>();
    private List<Vector3> localPos = new List<Vector3>();
    private CpmareInstanceID compInstanceID = new CpmareInstanceID();

    public void ClearAnchors()
    {
        anchors.Clear();
    }
    public void AddAnchor(GameObject anchor)
    {
        //same Anchor check
        if (-1 < anchors.BinarySearch(anchor, compInstanceID)) return;
 
        anchors.Add(anchor);
        anchors.Sort(compInstanceID);
        localPos.Add(anchor.transform.InverseTransformPoint(transform.position));
        
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        int validCount = 0;
        Vector3 pos = Vector3.zero;
        for(int i=0;i<anchors.Count;i++)
        {
            if(anchors[i].GetComponent<Anchor>().TrackingState == TrackingState.Tracking)
            {
                validCount++;
                pos += anchors[i].transform.TransformPoint(localPos[i]);
            }
        }
        if (validCount > 0)
        {
            pos /= validCount;
            this.transform.position = pos;
        }
		
	}
}
