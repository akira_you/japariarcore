﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIData : MonoBehaviour {
    public ARCoreCont arcont;
    public enum UIStates { ANCHOR,FLAG,READY,RUNNING }
    public Canvas MenuANCHOR;
    public Canvas MenuFLAG;
    public Canvas MenuREADY;
    public Canvas MenuRUN;
    public Planner planner;

    private UIStates _state=UIStates.ANCHOR;
    public UIStates State
    {
        get
        {
            return _state;
        }
        set
        {
            //propergate to each module 
            switch (value)
            {
                case UIStates.ANCHOR:
                    arcont.Mode = ARCoreCont.Modes.ANCHOR;
                    break;
                case UIStates.FLAG:
                    arcont.Mode = ARCoreCont.Modes.FLAG;
                    break;
                default:
                    arcont.Mode = ARCoreCont.Modes.NONE;
                    break;
            }

            switch (value)
            {
                case UIStates.READY:
                    planner.ShowState = true;
                    planner.MotorOn = false;
                    break;
                case UIStates.RUNNING:
                    planner.ShowState = true;
                    planner.MotorOn = true;
                    break;
                default:
                    planner.ShowState = false;
                    planner.MotorOn = false;
                    break;
            }


            //Change UI layer here.
            switch (value)
            {
                case UIStates.ANCHOR:
                    MenuANCHOR.gameObject.SetActive(true);
                    MenuFLAG.gameObject.SetActive(  false);
                    MenuREADY.gameObject.SetActive( false);
                    MenuRUN.gameObject.SetActive(   false);
                    break;
                case UIStates.FLAG:
                    MenuANCHOR.gameObject.SetActive(false);
                    MenuFLAG.gameObject.SetActive(  true);
                    MenuREADY.gameObject.SetActive(false);
                    MenuRUN.gameObject.SetActive(   false);
                    break;
                case UIStates.READY:
                    MenuANCHOR.gameObject.SetActive(false);
                    MenuFLAG.gameObject.SetActive(  false);
                    MenuREADY.gameObject.SetActive( true);
                    MenuRUN.gameObject.SetActive(   false);
                    break;

                default://RUN
                    MenuANCHOR.gameObject.SetActive(false);
                    MenuFLAG.gameObject.SetActive(  false);
                    MenuREADY.gameObject.SetActive( false);
                    MenuRUN.gameObject.SetActive(   true);
                    break;
            }

            _state = value;
        }
    }



    
    public void StateANCHOR()
    {
        State = UIStates.ANCHOR;
    }
    public void StatFLAG()
    {
        State = UIStates.FLAG;
    }
    public void StatREADY()
    {
        State = UIStates.READY;
    }
    public void StateRUN()
    {
        State = UIStates.RUNNING;
    }


    // Use this for initialization
    void Start () {
        State = UIStates.ANCHOR;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
